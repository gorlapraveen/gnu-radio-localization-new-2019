#!/bin/bash
 #example when the main.sh(this script) is initiated with following "./main.sh R1 1.5" means taking measurement of of receiver(R1) and distance from transmitter from being at 1.5 metere
if [[ $1 == R1 ]]; then
 echo "Executing in GNU Radio R1 Mode"
 case "$2" in
  0)
  echo "executing R2 at generic mode"
  echo "probably at 0 m"
  echo "Find the results [instantanesous] at "
  echo "For pacp :  pcap/wifi_pluto_rx_general.pcap"
  echo "For Results: results/wifi_pluot_rx_RSSI_general.log "
  bash -c "gnuradio-companion wifi_pluto_rx_general.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx_general.pcap >results/wifi_pluot_rx_RSSI_general.log"
  ;;
  0.25)
  echo "at 0.25m "
  bash -c "gnuradio-companion wifi_pluto_rx1_0_25.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx1_0_25.pcap >results/wifi_pluto_rx1_RSSI_0_25.log"
  ;;
  0.5)
  echo "at 0.5m"
  bash -c "gnuradio-companion wifi_pluto_rx1_0_5.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx1_0_5.pcap >results/wifi_pluto_rx1_RSSI_0_5.log"
  ;;
  0.75)
  echo "at 0.75m"
  bash -c "gnuradio-companion wifi_pluto_rx1_0_75.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx1_0_75.pcap >results/wifi_pluto_rx1_RSSI_0_75.log"
  ;;
  1)
  echo "at 1m"
  bash -c "gnuradio-companion wifi_pluto_rx1_1_0.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx1_1_0.pcap >results/wifi_pluto_rx1_RSSI_1_0.log"
  ;;
#######Next for 1.25 to 2 ################################################### 
  1.25)
  echo "at 1.25m"
  bash -c "gnuradio-companion wifi_pluto_rx1_1_25.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx1_1_25.pcap >results/wifi_pluto_rx1_RSSI_1_25.log"
  ;;
  1.5)
  echo "at 1.5m"
  bash -c "gnuradio-companion wifi_pluto_rx1_1_5.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx1_1_5.pcap >results/wifi_pluto_rx1_RSSI_1_5.log"
  ;;
  1.75)
  echo "at 1.75m"
  bash -c "gnuradio-companion wifi_pluto_rx1_1_75.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx1_1_75.pcap >results/wifi_pluto_rx1_RSSI_1_75.log"
  ;;
  2)
  echo "at 2m"
  bash -c "gnuradio-companion wifi_pluto_rx1_2_0.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx1_2_0.pcap >results/wifi_pluto_rx1_RSSI_2_0.log"
  ;;
#######Next for 2.25 to 3 ################################################### 
  2.25)
  echo "at 2.25m"
  bash -c "gnuradio-companion wifi_pluto_rx1_2_25.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx1_2_25.pcap >results/wifi_pluto_rx1_RSSI_2_25.log"
  ;;
  2.5)
  echo "at 2.5m"
  bash -c "gnuradio-companion wifi_pluto_rx1_2_5.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx1_2_5.pcap >results/wifi_pluto_rx1_RSSI_2_5.log"
  ;;
  2.75)
  echo "at 2.75m"
  bash -c "gnuradio-companion wifi_pluto_rx1_2_75.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx1_2_75.pcap >results/wifi_pluto_rx1_RSSI_2_75.log"
  ;;
  3)
  echo "at 3m"
  bash -c "gnuradio-companion wifi_pluto_rx1_3_0.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx1_3_0.pcap >results/wifi_pluto_rx1_RSSI_3_0.log"
  ;;
#######Next for 3.25 to 4 ################################################### 
  3.25)
  bash "at 3.25m"
  bash -c "gnuradio-companion wifi_pluto_rx1_3_25.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx1_3_25.pcap >results/wifi_pluto_rx1_RSSI_3_25.log"
  ;;
  3.5)
  echo "at 3.5m"
  bash -c "gnuradio-companion wifi_pluto_rx1_3_5.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx1_3_5.pcap >results/wifi_pluto_rx1_RSSI_3_5.log"
  ;;
  3.75)
  echo "at 3.75m"
  bash -c "gnuradio-companion wifi_pluto_rx1_3_75.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx1_3_75.pcap >results/wifi_pluto_rx1_RSSI_3_75.log"
  ;;
  4)
  echo "at 4m"
  bash -c "gnuradio-companion wifi_pluto_rx1_4_0.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx1_4_0.pcap >results/wifi_pluto_rx1_RSSI_4_0.log"
  ;;
#######Next for 4.25 to 5 ################################################### 
  4.25)
  echo "at 4.25m"
  bash -c "gnuradio-companion wifi_pluto_rx1_4_25.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx1_4_25.pcap >results/wifi_pluto_rx1_RSSI_4_25.log"
  ;;
  4.5)
  echo "at 4.5m"
  bash -c "gnuradio-companion wifi_pluto_rx1_4_5.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx1_4_5.pcap >results/wifi_pluto_rx1_RSSI_4_5.log"
  ;;
  4.75)
  bash "at 4.75m"
  bash -c "gnuradio-companion wifi_pluto_rx1_4_75.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx1_4_75.pcap >results/wifi_pluto_rx1_RSSI_4_75.log"
  ;;
  5)
  echo "at 5m"
  bash -c "gnuradio-companion wifi_pluto_rx1_5_0.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx1_5_0.pcap >results/wifi_pluto_rx1_RSSI_5_0.log"
  ;;
 esac
###Now In Other R2 mode
####Next Executing in R2 Mode######
elif [[ $1 == R2 ]]; then
 echo "Executing in GNU Radio R2 mode"
 case "$2" in
  0)
  echo "executing R2 at generic mode"
  echo "probably at 0 m"
  echo "Find the results [instantanesous] at "
  echo "For pacp :  pcap/wifi_pluto_rx_general.pcap"
  echo "For Results: results/wifi_pluot_rx_RSSI_general.log "
  bash -c "gnuradio-companion wifi_pluto_rx_general.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx_general.pcap >results/wifi_pluot_rx_RSSI_general.log"
  ;;
  0.25)
  echo "at 0.25m"
  bash -c "gnuradio-companion wifi_pluto_rx2_0_25.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx2_0_25.pcap >results/wifi_pluto_rx2_RSSI_0_25.log"
  ;;
  0.5)
  echo "at 0.5m"
  bash -c "gnuradio-companion wifi_pluto_rx2_0_5.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx2_0_5.pcap >results/wifi_pluto_rx2_RSSI_0_5.log"
  ;;
  0.75)
  echo "at 0.75m"
  bash -c "gnuradio-companion wifi_pluto_rx2_0_75.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx2_0_75.pcap >results/wifi_pluto_rx2_RSSI_0_75.log"
  ;;
  1)
  echo "at 1m"
  bash -c "gnuradio-companion wifi_pluto_rx2_1_0.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx2_1_0.pcap >results/wifi_pluto_rx2_RSSI_1_0.log"
  ;;
#######Next for 1.25 to 2 ################################################### 
  1.25)
  echo "at 1.25m"
  bash -c "gnuradio-companion wifi_pluto_rx2_1_25.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx2_1_25.pcap >results/wifi_pluto_rx1_RSSI_1_25.log"
  ;;
  1.5)
  echo "at 1.5m"
  bash -c "gnuradio-companion wifi_pluto_rx2_1_5.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx2_1_5.pcap >results/wifi_pluto_rx2_RSSI_1_5.log"
  ;;
  1.75)
  echo "at 1.75m"
  bash -c "gnuradio-companion wifi_pluto_rx2_1_75.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx1_2_75.pcap >results/wifi_pluto_rx2_RSSI_1_75.log"
  ;;
  2)
  echo "at 2m"
  bash -c "gnuradio-companion wifi_pluto_rx2_2_0.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx2_2_0.pcap >results/wifi_pluto_rx2_RSSI_2_0.log"
  ;;
#######Next for 2.25 to 3 ################################################### 
  2.25)
  echo "at 2.25m"
  bash -c "gnuradio-companion wifi_pluto_rx2_2_25.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx2_2_25.pcap >results/wifi_pluto_rx2_RSSI_2_25.log"
  ;;
  2.5)
  echo "at 2.5m"
  bash -c "gnuradio-companion wifi_pluto_rx2_2_5.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx2_2_5.pcap >results/wifi_pluto_rx2_RSSI_2_5.log"
  ;;
  2.75)
  echo "at 2.75m"
  bash -c "gnuradio-companion wifi_pluto_rx2_2_75.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx2_2_75.pcap >results/wifi_pluto_rx2_RSSI_2_75.log"
  ;;
  3)
  echo "at 3m"
  bash -c "gnuradio-companion wifi_pluto_rx2_3_0.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx2_3_0.pcap >results/wifi_pluto_rx2_RSSI_3_0.log"
  ;;
#######Next for 3.25 to 4 ################################################### 
  3.25)
  echo "at 3.25m"
  bash -c "gnuradio-companion wifi_pluto_rx2_3_25.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx2_3_25.pcap >results/wifi_pluto_rx2_RSSI_3_25.log"
  ;;
  3.5)
  echo "at 3.5 m"
  bash -c "gnuradio-companion wifi_pluto_rx2_3_5.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx2_3_5.pcap >results/wifi_pluto_rx2_RSSI_3_5.log"
  ;;
  3.75)
  echo "at 3.75m"
  bash -c "gnuradio-companion wifi_pluto_rx2_3_75.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx2_3_75.pcap >results/wifi_pluto_rx2_RSSI_3_75.log"
  ;;
  4)
  echo "at 4m"
  bash -c "gnuradio-companion wifi_pluto_rx2_4_0.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx2_4_0.pcap >results/wifi_pluto_rx2_RSSI_4_0.log"
  ;;
#######Next for 4.25 to 5 ################################################### 
  4.25)
  echo "at 4.25 m"
  bash -c "gnuradio-companion wifi_pluto_rx2_4_25.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx2_4_25.pcap >results/wifi_pluto_rx2_RSSI_4_25.log"
  ;;
  4.5)
  echo "at 4.5m"
  bash -c "gnuradio-companion wifi_pluto_rx2_4_5.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx2_4_5.pcap >results/wifi_pluto_rx2_RSSI_4_5.log"
  ;;
  4.75)
  echo "at 4.75m"
  bash -c "gnuradio-companion wifi_pluto_rx2_4_75.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx2_4_75.pcap >results/wifi_pluto_rx2_RSSI_4_75.log"
  ;;
  5)
  echo "at 5m"
  bash -c "gnuradio-companion wifi_pluto_rx2_5_0.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx2_5_0.pcap >results/wifi_pluto_rx2_RSSI_5_0.log"
  ;;
 esac
elif [[ $1 == AR1 ]]; then
 echo "Executing in GNU Radio R1, Aggressive Mode,  Without Obstracle"
 case "$2" in
  1) 
  echo "At 1m aggressive"
  bash -c "gnuradio-companion wifi_pluto_rx_at_1m_aggressive_LOS.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx_at_1m_aggressive_LOS.pcap >results/wifi_pluto_rx_at_1m_aggressive_LOS.log"
  ;;
 esac
elif [[ $1 == AR2 ]]; then
 echo "Executing in GNU Radio R2, Aggressive Mode, Obstracle"
 case "$2" in
  1) 
  echo "At 1m aggressive"
  bash -c "gnuradio-companion wifi_pluto_rx_at_1m_aggressive_OBS.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx_at_1m_aggressive_OBS.pcap >results/wifi_pluto_rx_at_1m_aggressive_OBS.log"
  ;;
 esac
######Next is Dynamic Mode for Receiver 1 and also Receiver 2, but first we need aggressive samples at 1m for each receiver also###################
elif [[ $1 == ADR1 ]]; then
 echo "Execting in Dynamic Mode for R1 receiver"
 case "$2" in
  1)
  echo "Dynamic Mode Aggressive at 1m for R1 receiver"
  bash -c "gnuradio-companion wifi_pluto_rx1_at_1m_aggressive_dynamic.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx1_at_1m_aggressive_dynamic.pcap >results/wifi_pluto_rx1_at_1m_aggressive_dynamic.log"
  ;;
 esac
 case "$2" in
  DM)
  echo "Dynamic Mode Aggressive at All distances for R1 receiver"
  bash -c "gnuradio-companion wifi_pluto_rx1_at_all_aggressive_dynamic.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx1_at_all_aggressive_dynamic.pcap >results/wifi_pluto_rx1_at_all_aggressive_dynamic.log"
  ;;
 esac
elif [[ $1 == ADR2 ]]; then
 echo "Execting in Dynamic Mode for R2 receiver"
 case "$2" in
  1)
  echo "Dynamic Mode Aggressive at 2m for R2 receiver"
  bash -c "gnuradio-companion wifi_pluto_rx2_at_1m_aggressive_dynamic.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx2_at_1m_aggressive_dynamic.pcap >results/wifi_pluto_rx2_at_1m_aggressive_dynamic.log"
  ;;
 esac
 case "$2" in
  DM)
  echo "Dynamic Mode Aggressive at All distances for R2 receiver"
  bash -c "gnuradio-companion wifi_pluto_rx2_at_all_aggressive_dynamic.grc"
  bash -c "tshark -r pcap/wifi_pluto_rx2_at_all_aggressive_dynamic.pcap >results/wifi_pluto_rx2_at_all_aggressive_dynamic.log"
  ;;
 esac 
else
 echo "No input matched"
 echo "executing R at generic mode probably at 0 m"
 bash -c "gnuradio-companion wifi_pluto_rx_general.grc"
 bash -c "tshark -r pcap/wifi_pluto_rx_general.pcap >results/wifi_pluot_rx_RSSI_general.log"
fi

