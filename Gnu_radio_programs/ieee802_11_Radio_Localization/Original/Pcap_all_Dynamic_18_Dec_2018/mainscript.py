
##Processing the RSSI values from the file of GNU RAdio ieee802.11 generated files to claculate the satndard deviation and for precesion adjustments
##Author Gorla Praveen
from ODSReader import ODSReader
import numpy as np
import math as mt
import time as tm
import os as os
import subprocess
RS_Calb_array=[]
RS_recv_lines_append=[]
Time_real_array=[]
RSSI_real_array=[]

#print os.path()
###Inital calibration using known samples of RSSI at distance 1m 
#RS_Calb=open("/home/debian/Documents/test.txt","r")
RS_Calb=open("sample_calibration_aggressive.txt","r")
for lines in RS_Calb.readlines():
   for i in lines.split():
      RS_Calb_array.append(float(i))
StDev_Calb=np.std(RS_Calb_array)
#print RS_Calb_array
#print StDev_Calb
RS_Calb_min_With_StD=np.mean(RS_Calb_array)-StDev_Calb
RS_Calb_max_With_StD=np.mean(RS_Calb_array)+StDev_Calb
#print "The Min RSSI at distance 1m is = ", RS_Calb_min_With_StD
#print "The Max RSSI at Distance 1m is = ", RS_Calb_max_With_StD
######Now Fetching  Instantaneous RSSI values and calculating   Standard deviations and the mean
#RS_recv=open("/home/debian/Documents/test.txt","r")


#######################
n=2.5
#RS_recv=open("sample_realtime_aggressive.txt","r")
#RS_recv_readlines=RS_recv.readlines()
#for RS_Real_lines in RS_recv_readlines:
#  Distance_recv=mt.pow(10,((RS_Calb_min_With_StD)-(RS_Real_lines))/(10*n))
doc = ODSReader(u'example.ods', clonespannedcolumns=True)
table = doc.getSheet(u'Sheet1')
for i in range(len(table)):
   #|--Column--0--|--Column--1--|--Column2--|
   #|-------------|-------------|-----------|
   #|-------------|-------------|-----------|
   Time_real_array=table[i][0]  #accessing elements in the column 0
   RSSI_real_array=table[i][1]  #accessing elements in the column 1
   #print Time_real_array
   #print RSSI_real_array
   Distance_recv=mt.pow(10,((RS_Calb_min_With_StD)-(float(RSSI_real_array)))/(10*n))   
   Distance_recv=np.array(Distance_recv)
   distance_file=open("distance_real.txt","a+")
   distance_file.write(str(Distance_recv)+'\n')
   distance_file.flush()
   distance_file.close()
   time_file=open("time_real.txt","a+")
   time_file.write(str(Time_real_array)+'\n')
   time_file.flush()
   time_file.close()
   distance_file=open("distance_real.txt","a+")
   distance_file_list=list(distance_file.readlines())
   distance_file.close()
   time_file.close()
   if float(len(distance_file_list))==0 : 
#When this is a first sample of the list, so there wont be any previous sample of distance or time, so making velocity and time is necessary to zero, because an error may occur when there is no previous value, for example len[Distance]-1 and len[distance-2] results in error as there is only single value.
      dt=0
      time_diff=open("time_diff.txt", "a+")
      time_diff.write(str(dt)+'\n')
      velocity_real=0
      velocity_real_file=open("velocity_real.txt","a+")
      velocity_real_file.write(str(velocity_real)+'\n')
      velocity_real_file.flush()
   elif float(len(distance_file_list))==1:
      dt=0
      time_diff=open("time_diff.txt", "a+")
      time_diff.write(str(dt)+'\n')
      velocity_real=0
      velocity_real_file=open("velocity_real.txt","a+")
      velocity_real_file.write(str(velocity_real)+'\n')
      velocity_real_file.flush()
   else:
      distance_file=open("distance_real.txt", "a+")
      distance_file_list=list(distance_file.readlines())
      D1=distance_file_list[len(distance_file_list)-1]
      D2=distance_file_list[len(distance_file_list)-2]
      time_file=open("time_real.txt", "a+")
      time_file_list=list(time_file.readlines())
      T1=time_file_list[len(time_file_list)-1]
      T2=time_file_list[len(time_file_list)-2]
      dt=float(T2)-float(T1)
      velocity_real=((float(D2))-(float(D1)))/((float(T2))-(float(T1)))
      time_diff=open("time_diff.txt","a+")
      time_diff.write(str(dt)+'\n')
      time_diff.flush()
      velocity_real_file=open("velocity_real.txt","a+")
      velocity_real_file.write(str(abs(velocity_real))+'\n')
      velocity_real_file.flush()
      print "Instantaneous Realtime Velocity= "+ str(abs(velocity_real))
      distance_file.close()
      time_file.close()
      subprocess.call(['python', 'kalman_filter_design.py'])
      
     

