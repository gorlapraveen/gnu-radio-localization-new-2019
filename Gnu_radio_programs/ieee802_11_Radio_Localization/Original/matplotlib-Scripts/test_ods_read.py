from ODSReader import ODSReader
import matplotlib.pyplot as plt
import numpy as np
RSSIArray = []
FERArray = []
ProbArray = []
doc = ODSReader(u'example.ods', clonespannedcolumns=True)
table = doc.getSheet(u'Sheet1')
N = len(table)
for i in range(len(table)):
   FERArray= np.append(FERArray, table[i][0]) #accessing elements in the 0 column
   RSSIArray= np.append(RSSIArray,table[i][1]) #Accessing elements in the  1 column
   ProbArray= np.append(ProbArray, table[i][2]) #accessing elements in the 2 column
print(FERArray)
print(RSSIArray)
print(ProbArray)
ind = np.arange(N)
FERArray=FERArray.astype(np.float) #converting string array to float
RSSIArray=RSSIArray.astype(np.float) #converting string array to float
ProbArray=ProbArray.astype(np.float) #converting string array to float
p1=plt.scatter(ind, RSSIArray, 1)
plt.ylabel('ylabel')
plt.title('Title')
plt.xticks(ind, FERArray)
plt.yticks(np.arange(16, 18.5, 0.2))
plt.show()

p2=plt.scatter(ind, ProbArray,1)
plt.ylabel('ylabel')
plt.title('Title')
plt.xticks(ind, FERArray)
plt.yticks(np.arange(0, 1, 0.5))
plt.show()
