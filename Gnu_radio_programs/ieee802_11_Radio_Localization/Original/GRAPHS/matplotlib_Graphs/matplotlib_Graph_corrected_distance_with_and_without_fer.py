from ODSReader import ODSReader
import matplotlib.pyplot as plt
import numpy as np
ActualDistance= []
EvaluatedCorrectedDistance= []
EvaluatedUnCorrectedDistance= []
ErrorEvaluatedCorrectedDistance= []
ErrorEvaluatedUnCorrectedDistance= []
ErrorEvaluatedCorrectedDistancePercentage= []
ErrorEvaluatedUnCorrectedDistancePercentage= []

ErrorCorrected= np.array([0.2857, 4.3013,  21.0487]).astype(np.double)

ErrorUnCorrected= np.array([2.0000, 7.8318, 16.7395,]).astype(np.double)

ErrorText=np.array(['Minimium', 'Average', 'Maximum'])

#ProbArray = []

####################################################For 1###############################
doc = ODSReader(u'Corrected_Distance_with_and_without_FER_correction.ods', clonespannedcolumns=True)
table = doc.getSheet(u'Sheet1')
N = len(table)
for i in range(len(table)):
   ActualDistance= np.append(ActualDistance, table[i][0]) #accessing elements in the 0 column
   EvaluatedCorrectedDistance= np.append(EvaluatedCorrectedDistance, table[i][2])
   EvaluatedUnCorrectedDistance= np.append(EvaluatedUnCorrectedDistance, table[i][4])
   ErrorEvaluatedCorrectedDistance= np.append(ErrorEvaluatedCorrectedDistance, table[i][5])
   ErrorEvaluatedUnCorrectedDistance= np.append(ErrorEvaluatedUnCorrectedDistance, table[i][6])
   ErrorEvaluatedCorrectedDistancePercentage= np.append(ErrorEvaluatedCorrectedDistancePercentage, table[i][7])
   ErrorEvaluatedUnCorrectedDistancePercentage= np.append(ErrorEvaluatedUnCorrectedDistancePercentage, table[i][8])
   
print N
ind = np.arange(N)

#ActualDistance= np.asarray(ActualDistance).astype(np.double)
#EvaluatedCorrectedDistance= np.asarray(EvaluatedCorrectedDistance)
#EvaluatedUnCorrectedDistance= np.asarray(EvaluatedUnCorrectedDistance)




#PowerAnalysis_Distance=PowerAnalysis_Distance.astype(np.float) #converting string array to float
ActualDistance= ActualDistance.astype(np.double)
ActualDistanceMask= np.isfinite(ActualDistance)
EvaluatedCorrectedDistance= EvaluatedCorrectedDistance.astype(np.double)
EvaluatedCorrectedDistanceMask= np.isfinite(EvaluatedCorrectedDistance)
EvaluatedUnCorrectedDistance= EvaluatedUnCorrectedDistance.astype(np.double)
EvaluatedUnCorrectedDistanceMask= np.isfinite(EvaluatedUnCorrectedDistance)

ErrorEvaluatedCorrectedDistance= ErrorEvaluatedCorrectedDistance.astype(np.double)
ErrorEvaluatedUnCorrectedDistance= ErrorEvaluatedUnCorrectedDistance.astype(np.double)
ErrorEvaluatedCorrectedDistancePercentage= ErrorEvaluatedCorrectedDistancePercentage.astype(np.double)
ErrorEvaluatedUnCorrectedDistancePercentage= ErrorEvaluatedUnCorrectedDistancePercentage.astype(np.double)

########################### Plotting ##################################

#p1=plt.plot(ind, EvaluatedUnCorrectedDistance, label='Without FER based Correction', marker='o' )
#p12=plt.plot(ind, EvaluatedCorrectedDistance, label='With FER based Correction', marker='o')
p1=plt.plot(ind[EvaluatedCorrectedDistanceMask], EvaluatedUnCorrectedDistance[EvaluatedCorrectedDistanceMask], label='Without FER based Correction', linestyle='-',marker='D' )
p12=plt.plot(ind[EvaluatedUnCorrectedDistanceMask], EvaluatedCorrectedDistance[EvaluatedUnCorrectedDistanceMask], linestyle='-.', label='With FER based Correction', marker='o')
plt.ylabel('Estimated distance (m)')
plt.xlabel('Actual distance (m)')
plt.xticks(ind[ActualDistanceMask], ActualDistance[ActualDistanceMask])
plt.yticks(np.arange(0,5,0.5))
#plt.xticks(ind, ActualDistance)
#plt.yticks(np.arange(0,5,0.5))
plt.autoscale(enable=True, axis='both', tight=True)
#start, end = plt.gca().get_xlim()
#print start
#print end
#plt.gca().xaxis.set_ticks(np.arange(start, end, 0.25))
plt.legend()
plt.grid()
plt.show()

p2=plt.bar(ind, ErrorEvaluatedUnCorrectedDistance, label='Without FER based Correction', alpha=0.2)
p22=plt.bar(ind, ErrorEvaluatedCorrectedDistance, label='With FER based Correction', width=0.4, alpha=0.85)
plt.ylabel('Estimated error (m)')
plt.xlabel('Actual distance (m)')
plt.xticks(ind, ActualDistance)
plt.yticks(np.arange(0,1,0.2))
plt.legend()
plt.grid()
plt.show()

p3=plt.bar(ind, ErrorEvaluatedUnCorrectedDistancePercentage, label='Without FER based Correction', alpha=0.2)
p32=plt.bar(ind, ErrorEvaluatedCorrectedDistancePercentage, label='With FER based Correction', width=0.4, alpha=0.85)
plt.ylabel('Estimated error (%)')
plt.xlabel('Actual distance (m)')
plt.xticks(ind, ActualDistance)
plt.yticks(np.arange(0,30,5))
plt.legend()
plt.grid()
plt.show()
 

p4=plt.bar(np.arange(3), ErrorUnCorrected, label='Without FER based Correction', alpha=0.2, width=0.12)
p42=plt.bar(np.arange(3), ErrorCorrected, label='With FER based Correction', width=0.06, alpha=0.85)
plt.ylabel('Estimated error (%)')
plt.xticks(np.arange(3), ErrorText)
plt.yticks(np.arange(0,30,5))
plt.legend()
plt.grid()
plt.show()

