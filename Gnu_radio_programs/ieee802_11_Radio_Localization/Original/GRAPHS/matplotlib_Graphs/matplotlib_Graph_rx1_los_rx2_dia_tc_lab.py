from ODSReader import ODSReader
import matplotlib.pyplot as plt
import numpy as np
ActualRx1Distance= []
EvaluatedRx1Distance= []
ActualRx2Distance= []
EvaluatedRx2Distance= []

#ProbArray = []

####################################################For 1###############################
doc = ODSReader(u'Rx1_Los_Rx2_Dia_TCLab.ods', clonespannedcolumns=True)
table = doc.getSheet(u'Sheet1')
N = len(table)
for i in range(len(table)):
   ActualRx1Distance= np.append(ActualRx1Distance, table[i][0]) #accessing elements in the 0 column
   EvaluatedRx1Distance= np.append(EvaluatedRx1Distance, table[i][1])

doc1 = ODSReader(u'Rx1_Los_Rx2_Dia_TCLab.ods', clonespannedcolumns=True)
table1 = doc1.getSheet(u'Sheet2')
N1=len(table1)
for i in range(len(table1)):
   ActualRx2Distance= np.append(ActualRx2Distance, table1[i][0])
   EvaluatedRx2Distance= np.append(EvaluatedRx2Distance, table1[i][1])
   

ind = np.arange(N)
ind1=np.arange(N1)
#PowerAnalysis_Distance=PowerAnalysis_Distance.astype(np.float) #converting string array to float
ActualRx1Distance= ActualRx1Distance.astype(np.float)
EvaluatedRx1Distance= EvaluatedRx1Distance.astype(np.float)
ActualRx2Distance= ActualRx2Distance.astype(np.float)
EvaluatedRx2Distance= EvaluatedRx2Distance.astype(np.float)

#EvaluatedDistance_2_7=EvaluatedDistance_2_7.astype(np.float)
print ActualRx2Distance

########################### Plotting ##################################

p1=plt.plot(ind, EvaluatedRx1Distance, marker='o' )
plt.ylabel('Estimated distance (m)')
plt.xlabel('Actual distance between Tx and Rx1 (m)')
plt.xticks(ind, ActualRx1Distance)
plt.yticks(np.arange(0,6,1))
plt.legend()
plt.grid()
plt.show()

p2=plt.plot(ind1, EvaluatedRx2Distance, marker='o' )
plt.ylabel('Estimated distance (m)')
plt.xlabel('Actual distance between Tx and Rx2 (m)')
plt.xticks(ind, ActualRx2Distance)
plt.yticks(np.arange(3,6,1))
plt.legend()
plt.grid()
plt.show()



