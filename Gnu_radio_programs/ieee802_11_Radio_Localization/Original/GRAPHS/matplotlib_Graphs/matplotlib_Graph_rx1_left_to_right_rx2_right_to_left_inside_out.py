from ODSReader import ODSReader
import matplotlib.pyplot as plt
import numpy as np
DistanceFrom0to5= []
ActualRx1Distance= []
EvaluatedRx1Distance= []
ActualRx2Distance= []
EvaluatedRx2Distance= []

#ProbArray = []

####################################################For 1###############################
doc = ODSReader(u'Rx1_left_to_right_Rx2_right_to_left_inside_outside.ods', clonespannedcolumns=True)
table = doc.getSheet(u'Sheet1')
N = len(table)
for i in range(len(table)):
   DistanceFrom0to5= np.append(DistanceFrom0to5, table[i][0])
   ActualRx1Distance= np.append(ActualRx1Distance, table[i][1]) #accessing elements in the 0 column
   EvaluatedRx1Distance= np.append(EvaluatedRx1Distance, table[i][2])
   ActualRx2Distance= np.append(ActualRx2Distance, table[i][3])
   EvaluatedRx2Distance= np.append(EvaluatedRx2Distance, table[i][4])
   

ind = np.arange(N)
#PowerAnalysis_Distance=PowerAnalysis_Distance.astype(np.float) #converting string array to float
DistanceFrom0to5= DistanceFrom0to5.astype(np.float)
ActualRx1Distance= ActualRx1Distance.astype(np.float)
EvaluatedRx1Distance= EvaluatedRx1Distance.astype(np.float)
ActualRx2Distance= ActualRx2Distance.astype(np.float)
EvaluatedRx2Distance= EvaluatedRx2Distance.astype(np.float)

#EvaluatedDistance_2_7=EvaluatedDistance_2_7.astype(np.float)

########################### Plotting ##################################

p1=plt.plot(ind, ActualRx1Distance, label='Actual distance of Tx from Rx1', marker='o' )
p12=plt.plot(ind, EvaluatedRx1Distance, label='Estimated distance of Tx from Rx1', marker='o')
plt.ylabel('Distance (m)')
plt.xlabel('Tx moving from 0 (m) to 5 (m)')
plt.xticks(ind, DistanceFrom0to5)
plt.yticks(np.arange(0,7,1))
plt.legend()
plt.grid()
plt.show()


p2=plt.plot(ind, ActualRx2Distance, label='Actual distance of Tx from Rx2', marker='o' )
p22=plt.plot(ind, EvaluatedRx2Distance, label='Estimated distance of Tx from Rx2', marker='o')
plt.ylabel('Distance (m)')
plt.xlabel('Tx moving from 0 (m) to 5 (m)')
plt.xticks(ind, DistanceFrom0to5)
plt.yticks(np.arange(0,7,1))
plt.legend()
plt.grid()
plt.show()


