from ODSReader import ODSReader
import matplotlib.pyplot as plt
import numpy as np
Distance= []
MinFER= []
MaxFER= []
AvgFER= []
VarFER= []

#ProbArray = []

####################################################For 1###############################
doc = ODSReader(u'FER_LOS_Aggressive_RX1.ods', clonespannedcolumns=True)
table = doc.getSheet(u'Sheet1')
N = len(table)
for i in range(len(table)):
   Distance= np.append(Distance, table[i][0]) #accessing elements in the 0 column
   MinFER= np.append(MinFER, table[i][1])
   MaxFER= np.append(MaxFER, table[i][2])
   AvgFER= np.append(AvgFER, table[i][3])
   VarFER= np.append(VarFER, table[i][4])


ind = np.arange(N)
#PowerAnalysis_Distance=PowerAnalysis_Distance.astype(np.float) #converting string array to float
Distance=Distance.astype(np.float)
MinFER=MinFER.astype(np.float)
MaxFER=MaxFER.astype(np.float)
AvgFER=AvgFER.astype(np.float)
VarFER=VarFER.astype(np.float)
#EvaluatedDistance_2_7=EvaluatedDistance_2_7.astype(np.float)

#ProbArray=ProbArray.astype(np.float) #converting string array to float



########################### Plotting ##################################

p1=plt.plot(ind, AvgFER,label='Average FER', marker='o' )
p12=plt.plot(ind, MaxFER,label='Variance FER', marker='o' )
plt.ylabel('FER')
plt.xlabel('Distance(m)')
plt.xticks(ind, Distance)
plt.yticks(np.arange(0,1.5,0.4))
plt.legend()
plt.grid()
plt.show()



