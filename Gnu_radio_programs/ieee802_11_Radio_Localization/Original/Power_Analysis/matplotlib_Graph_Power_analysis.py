from ODSReader import ODSReader
import matplotlib.pyplot as plt
import numpy as np
PowerAnalysis_RSSIArray = []
PowerAnalysis_Distance = []
PowerAnalysis_RSSIArray_NoObstacle = []
PowerAnalysis_RSSI_NoObstacle = -41.4414
#ProbArray = []

PowerAnalysis_RSSIArray1 = []
PowerAnalysis_Distance1 = []
PowerAnalysis_RSSIArray_NoObstacle1 = []
PowerAnalysis_RSSI_NoObstacle_1 = -43.689
#ProbArray1 = []

PowerAnalysis_RSSIArray2 = []
PowerAnalysis_Distance2 = []
PowerAnalysis_RSSIArray_NoObstacle2 = []
PowerAnalysis_RSSI_NoObstacle_2 = -42.1
#ProbArray2 = []

####################################################For 1###############################
doc = ODSReader(u'power_analysis_1.ods', clonespannedcolumns=True)
table = doc.getSheet(u'Sheet1')
N = len(table)
for i in range(len(table)):
   PowerAnalysis_Distance= np.append(PowerAnalysis_Distance, table[i][0]) #accessing elements in the 0 column
   PowerAnalysis_RSSIArray= np.append(PowerAnalysis_RSSIArray,table[i][1]) #Accessing elements in the  1 column
   PowerAnalysis_RSSIArray_NoObstacle= np.append(PowerAnalysis_RSSIArray_NoObstacle, PowerAnalysis_RSSI_NoObstacle)
 #  ProbArray= np.append(ProbArray, table[i][2]) #accessing elements in the 2 column


###############################################For 2###########################
doc1 = ODSReader(u'power_analysis_2.ods', clonespannedcolumns=True)
table1 = doc1.getSheet(u'Sheet1')
N1 = len(table1)
for i in range(len(table1)):
   PowerAnalysis_Distance1= np.append(PowerAnalysis_Distance1, table1[i][0]) #accessing elements in the 0 column
   PowerAnalysis_RSSIArray1= np.append(PowerAnalysis_RSSIArray1,table1[i][1]) #Accessing elements in the  1 column
 #  ProbArray1= np.append(ProbArray1, table1[i][2]) #accessing elements in the 2 column
   PowerAnalysis_RSSIArray_NoObstacle1= np.append(PowerAnalysis_RSSIArray_NoObstacle1, PowerAnalysis_RSSI_NoObstacle_1)

###############################################For 3###########################
doc2 = ODSReader(u'power_analysis_3.ods', clonespannedcolumns=True)
table2 = doc2.getSheet(u'Sheet1')
N2 = len(table2)
for i in range(len(table2)):
   PowerAnalysis_Distance2= np.append(PowerAnalysis_Distance2, table2[i][0]) #accessing elements in the 0 column
   PowerAnalysis_RSSIArray2= np.append(PowerAnalysis_RSSIArray2,table2[i][1]) #Accessing elements in the  1 column
 #  ProbArray1= np.append(ProbArray1, table1[i][2]) #accessing elements in the 2 column
   PowerAnalysis_RSSIArray_NoObstacle2= np.append(PowerAnalysis_RSSIArray_NoObstacle2, PowerAnalysis_RSSI_NoObstacle_2)


#print(PowerAnalysis_Distance)
#print(PowerAnalysis_RSSIArray)
#print(ProbArray)


ind = np.arange(N)
#PowerAnalysis_Distance=PowerAnalysis_Distance.astype(np.float) #converting string array to float
PowerAnalysis_Distance=PowerAnalysis_Distance.astype(np.float)
PowerAnalysis_RSSIArray=PowerAnalysis_RSSIArray.astype(np.float) #converting string array to float
PowerAnalysis_RSSIArray_NoObstacle=PowerAnalysis_RSSIArray_NoObstacle.astype(np.float)
#ProbArray=ProbArray.astype(np.float) #converting string array to float



#PowerAnalysis_Distance1=PowerAnalysis_Distance1.astype(np.float) #converting string array to float
PowerAnalysis_Distance1=PowerAnalysis_Distance1.astype(np.float)
PowerAnalysis_RSSIArray1=PowerAnalysis_RSSIArray1.astype(np.float) #converting string array to float
PowerAnalysis_RSSIArray1_NoObstacle1=PowerAnalysis_RSSIArray_NoObstacle1.astype(np.float)
#ProbArray1=ProbArray1.astype(np.float) #converting string array to float
PowerAnalysis_Distance2=PowerAnalysis_Distance2.astype(np.float)
PowerAnalysis_RSSIArray2=PowerAnalysis_RSSIArray2.astype(np.float)
PowerAnalysis_RSSIArray_NoObstacle2=PowerAnalysis_RSSIArray_NoObstacle2.astype(np.float)

########################### Plotting ##################################

p1=plt.plot(ind, PowerAnalysis_RSSIArray,label='Obstacle', marker='o' )
p12=plt.plot(ind,PowerAnalysis_RSSIArray_NoObstacle,label='No Obstacle', marker='o' )
plt.ylabel('Rx (dBm) received')
plt.xlabel('Distance (cm) when obstacle moves from Tx to Rx')
#plt.title('Power Analysis Experiment 1 : When obstacle is placed at different distances from Tx and Rx (LOS)')
plt.xticks(ind, PowerAnalysis_Distance)
plt.yticks(np.arange(-55, -40, 2))
plt.legend()
plt.grid()
plt.show()


#####################################
p2=plt.plot(ind, PowerAnalysis_RSSIArray1,label='Obstacle', marker='o' )
p22=plt.plot(ind,PowerAnalysis_RSSIArray_NoObstacle1,label='No Obstacle', marker='o' )
plt.ylabel('Rx (dBm) received')
plt.xlabel('Distance (cm) when obstacle moves from Tx to Rx')
#plt.title('Power Analysis Experiment 2 : When obstacle is placed at different distances from Tx and Rx (LOS)')
plt.xticks(ind, PowerAnalysis_Distance1)
plt.yticks(np.arange(-55, -40, 2))
plt.legend()
plt.grid()
plt.show()
################
p3=plt.plot(ind, PowerAnalysis_RSSIArray,label='Obstacle', marker='o' )
p32=plt.plot(ind,PowerAnalysis_RSSIArray_NoObstacle2,label='No Obstacle', marker='o' )
plt.ylabel('Rx (dBm) received')
plt.xlabel('Distance (cm) when obstacle moves from Tx to Rx')
#plt.title('Power Analysis Experiment 3 : When obstacle is placed at different distances from Tx and Rx (LOS)')
plt.xticks(ind, PowerAnalysis_Distance2)
plt.yticks(np.arange(-55, -40, 2))
plt.legend()
plt.grid()
plt.show()
