from ODSReader import ODSReader
import matplotlib.pyplot as plt
import numpy as np
RSSIArray = []
FERArray = []
#ProbArray = []

RSSIArray1 = []
FERArray1 = []
#ProbArray1 = []

RSSIArray2 = []
FERArray2 = []
#ProbArray2 = []

####################################################For 1m_1###############################
doc = ODSReader(u'FER_RSSI_consolidated_1m_1_0-0_5-0_99.ods', clonespannedcolumns=True)
table = doc.getSheet(u'Sheet1')
N = len(table)
for i in range(len(table)):
   FERArray= np.append(FERArray, table[i][0]) #accessing elements in the 0 column
   RSSIArray= np.append(RSSIArray,table[i][1]) #Accessing elements in the  1 column
 #  ProbArray= np.append(ProbArray, table[i][2]) #accessing elements in the 2 column

###############################################For 1m_2###########################
doc1 = ODSReader(u'FER_RSSI_consolidated_1m_2_0-0_5-0_99.ods', clonespannedcolumns=True)
table1 = doc1.getSheet(u'Sheet1')
N1 = len(table1)
for i in range(len(table1)):
   FERArray1= np.append(FERArray1, table1[i][0]) #accessing elements in the 0 column
   RSSIArray1= np.append(RSSIArray1,table1[i][1]) #Accessing elements in the  1 column
 #  ProbArray1= np.append(ProbArray1, table1[i][2]) #accessing elements in the 2 column


############################################For 1m_1##############################
###############################################For 1m_2######https://s#####################
doc2 = ODSReader(u'FER_RSSI_consolidated_1m_3_0-0_5-0_99.ods', clonespannedcolumns=True)
table2 = doc2.getSheet(u'Sheet1')
N2 = len(table2)
for i in range(len(table2)):
   FERArray2= np.append(FERArray2, table2[i][0]) #accessing elements in the 0 column
   RSSIArray2= np.append(RSSIArray2,table2[i][1]) #Accessing elements in the  1 column
 #  ProbArray2= np.append(ProbArray2, table2[i][2]) #accessing elements in the 2 column

#print(FERArray)
#print(RSSIArray)
#print(ProbArray)


ind = np.arange(N)
#FERArray=FERArray.astype(np.float) #converting string array to float
RSSIArray=RSSIArray.astype(np.float) #converting string array to float
print "RSSI at 1m_1 =" 
print RSSIArray
#ProbArray=ProbArray.astype(np.float) #converting string array to float

#FERArray1=FERArray1.astype(np.float) #converting string array to float
RSSIArray1=RSSIArray1.astype(np.float) #converting string array to float
#ProbArray1=ProbArray1.astype(np.float) #converting string array to float
print "RSSI at 1m_2 ="  
print RSSIArray1
#FERArray2=FERArray2.astype(np.float) #converting string array to float
RSSIArray2=RSSIArray2.astype(np.float) #converting string array to float
#ProbArray2=ProbArray2.astype(np.float) #converting string array to float
print "RSSI at 1m_3 ="  
print RSSIArray2

########################### Plotting ##################################

p1=plt.bar(ind, RSSIArray, 0.1)
p2=plt.bar(ind, RSSIArray1, 0.1)
p3=plt.bar(ind, RSSIArray2, 0.06)
plt.ylabel('RSSI(dBm)')
plt.xlabel('FER(Frame Error Rate)')
plt.title('RSSI vs FER at 1m')
plt.xticks(ind, FERArray)
plt.yticks(np.arange(16, 20, 0.5))
plt.legend((p1, p2, p3), ('At 1m 1', 'At 1m 2','At 1m 3'))
plt.grid(True)
plt.show()

