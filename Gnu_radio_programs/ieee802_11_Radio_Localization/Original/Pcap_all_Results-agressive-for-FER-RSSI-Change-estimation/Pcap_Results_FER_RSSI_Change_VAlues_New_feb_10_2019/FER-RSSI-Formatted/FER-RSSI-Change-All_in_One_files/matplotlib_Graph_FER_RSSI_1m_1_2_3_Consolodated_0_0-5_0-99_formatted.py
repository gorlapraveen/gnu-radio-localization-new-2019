from ODSReader import ODSReader
import matplotlib.pyplot as plt
import numpy as np
RSSIArray = []
FERArray = []
#ProbArray = []

RSSIArray1 = []
FERArray1 = []
#ProbArray1 = []

RSSIArray2 = []
FERArray2 = []
#ProbArray2 = []

####################################################For 1m_1###############################
doc = ODSReader(u'FER_RSSI_consolidated_1m_1_0-0_5-0_99_formatted.ods', clonespannedcolumns=True)
table = doc.getSheet(u'Sheet1')
N = len(table)
for i in range(len(table)):
   FERArray= np.append(FERArray, table[i][0]) #accessing elements in the 0 column
   RSSIArray= np.append(RSSIArray,table[i][1]) #Accessing elements in the  1 column
 #  ProbArray= np.append(ProbArray, table[i][2]) #accessing elements in the 2 column

ind = np.arange(N)
#FERArray=FERArray.astype(np.float) #converting string array to float
RSSIArray=RSSIArray.astype(np.float) #converting string array to float
print "RSSI at 1m_1 =" 
print RSSIArray
#ProbArray=ProbArray.astype(np.float) #converting string array to float


########################### Plotting ##################################

p1=plt.bar(ind, RSSIArray, 0.1)
plt.ylabel('RSSI (dBm)')
plt.xlabel('Frame Error Rate (FER)')
plt.xticks(ind, FERArray)
plt.yticks(np.arange(16, 20, 0.2))
#plt.legend((p1), ('At 1m'))
plt.grid(True)
plt.show()

