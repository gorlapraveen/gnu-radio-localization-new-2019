from ODSReader import ODSReader
import matplotlib.pyplot as plt
import numpy as np
RSSIArray = []
FERArray = []
#ProbArray = []

RSSIArray1 = []
FERArray1 = []
#ProbArray1 = []

RSSIArray2 = []
FERArray2 = []
#ProbArray2 = []

####################################################For 2m_1###############################
doc = ODSReader(u'FER_RSSI_consolidated_2m_1_0-0_5-0_99.ods', clonespannedcolumns=True)
table = doc.getSheet(u'Sheet1')
N = len(table)
for i in range(len(table)):
   FERArray= np.append(FERArray, table[i][0]) #accessing elements in the 0 column
   RSSIArray= np.append(RSSIArray,table[i][1]) #Accessing elements in the  1 column
 #  ProbArray= np.append(ProbArray, table[i][2]) #accessing elements in the 2 column


###############################################For 2m_3###########################
doc2 = ODSReader(u'FER_RSSI_consolidated_2m_3_0-0_5-0_99.ods', clonespannedcolumns=True)
table2 = doc2.getSheet(u'Sheet1')
N2 = len(table2)
for i in range(len(table2)):
   FERArray2= np.append(FERArray2, table2[i][0]) #accessing elements in the 0 column
   RSSIArray2= np.append(RSSIArray2,table2[i][1]) #Accessing elements in the  1 column
 #  ProbArray2= np.append(ProbArray2, table2[i][2]) #accessing elements in the 2 column

#print(FERArray)
#print(RSSIArray)
#print(ProbArray)


ind = np.arange(N)
#FERArray=FERArray.astype(np.float) #converting string array to float
RSSIArray=RSSIArray.astype(np.float) #converting string array to float
#ProbArray=ProbArray.astype(np.float) #converting string array to float
print "RSSI Array"
print RSSIArray
print "RSSIArray 2"
print RSSIArray2


#FERArray2=FERArray2.astype(np.float) #converting string array to float
RSSIArray2=RSSIArray2.astype(np.float) #converting string array to float
#ProbArray2=ProbArray2.astype(np.float) #converting string array to float


########################### Plotting ##################################

p1=plt.bar(ind, RSSIArray, 0.1)
p3=plt.bar(ind, RSSIArray2, 0.05)
plt.ylabel('RSSI(dBm)')
plt.xlabel('FER(Frame Error Rate)')
plt.title('RSSI vs FER at 2m')
plt.xticks(ind, FERArray)
plt.yticks(np.arange(7, 20, 0.5))
plt.legend((p1, p3), ('At 2m 1','At 2m 3'))
#plt.grid()
plt.show()

